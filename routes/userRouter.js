const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('../helpers/asyncWrapper');
const {authMiddleware} = require('../middleware/authMiddleware');
const {
  getUserInfo,
  deleteUser,
  changeUserPassword,
} = require('../controllers/userController');

router.use(asyncWrapper(authMiddleware));

router.get('/', asyncWrapper(getUserInfo));
router.delete('/', asyncWrapper(deleteUser));
router.patch('/password', asyncWrapper(changeUserPassword));

module.exports = router;
