const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('../helpers/asyncWrapper');
const {
  register,
  login,
  forgotPassword,
} = require('../controllers/authController');

router.post('/register', asyncWrapper(register));
router.post('/login', asyncWrapper(login));
router.post('/forgot_password', asyncWrapper(forgotPassword));

module.exports = router;
