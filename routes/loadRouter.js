const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('../helpers/asyncWrapper');
const {authMiddleware} = require('../middleware/authMiddleware');
const {
  getUserLoads,
  addLoadForUser,
  getUserActiveLoad,
  iterateToNextLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingInfoById,
} = require('../controllers/loadController');

router.use(asyncWrapper(authMiddleware));

router.get('/', asyncWrapper(getUserLoads));
router.post('/', asyncWrapper(addLoadForUser));
router.get('/active', asyncWrapper(getUserActiveLoad));
router.patch('/active/state', asyncWrapper(iterateToNextLoadState));
router.get('/:id', asyncWrapper(getUserLoadById));
router.put('/:id', asyncWrapper(updateUserLoadById));
router.delete('/:id', asyncWrapper(deleteUserLoadById));
router.post('/:id/post', asyncWrapper(postUserLoadById));
router.get('/:id/shipping_info', asyncWrapper(getUserLoadShippingInfoById));

module.exports = router;
