const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('../helpers/asyncWrapper');
const {authMiddleware} = require('../middleware/authMiddleware');
const {
  getUsersTrucks,
  addTruckForUser,
  getUserTruckById,
  updateUserTruckById,
  deleteUserTruckById,
  assignTruckForUserById,
} = require('../controllers/truckController');

router.use(asyncWrapper(authMiddleware));

router.get('/', asyncWrapper(getUsersTrucks));
router.post('/', asyncWrapper(addTruckForUser));
router.get('/:id', asyncWrapper(getUserTruckById));
router.post('/:id/assign', asyncWrapper(assignTruckForUserById));
router.put('/:id', asyncWrapper(updateUserTruckById));
router.delete('/:id', asyncWrapper(deleteUserTruckById));

module.exports = router;
