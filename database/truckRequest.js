/* eslint-disable camelcase */
const {Truck} = require('../models/truckModel');
const {verifyBelong} = require('../helpers/dataVerification');

module.exports.dbTruckGetItems = async (created_by, skipLimit) => {
  return await Truck.find({created_by}, null, skipLimit);
};

module.exports.dbTruckAddItem = async (item) => {
  await new Truck(item).save();
};

module.exports.dbTruckGetItemById = async (id, userId) => {
  const item = await Truck.findById(id);
  verifyBelong(userId, item.created_by);

  return item;
};

module.exports.dbTruckUpdateItemById = async (id, userId, truck) => {
  const item = await Truck.findById(id);
  verifyBelong(userId, item.created_by);

  Object.keys(truck).forEach((key) => {
    item[key] = truck[key];
  });

  await item.save();
};

module.exports.dbTruckDeleteItemById = async (id, userId) => {
  const item = await Truck.findById(id);
  verifyBelong(userId, item.created_by);

  await item.remove();
};

module.exports.dbTruckAssignItemById = async (id, userId) => {
  const item = await Truck.findById(id);
  verifyBelong(userId, item.created_by);

  item.assigned_to = userId;

  const assignedTucks = await Truck.find({
    created_by: userId,
    assigned_to: userId,
  });
  assignedTucks.forEach(async (truck) => {
    truck.assigned_to = null;
    await truck.save();
  });

  await item.save();
};
