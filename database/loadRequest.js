/* eslint-disable camelcase */
const {Truck} = require('../models/truckModel');
const {Load} = require('../models/loadModel');
const {verifyBelong} = require('../helpers/dataVerification');
const {BadRequestError} = require('../Errors');
const config = require('config');
const TRUCK_TYPE = config.get('TRUCK_TYPE');
const LOAD_STATE = config.get('LOAD_STATE');

module.exports.dbLoadGetItems = async (created_by, skipLimit, status) => {
  console.log(created_by, skipLimit, status);
  if (status) {
    return await Load.find({created_by, status}, null, skipLimit);
  } else {
    return await Load.find({created_by}, null, skipLimit);
  }
};

module.exports.dbLoadAddItem = async (item) => {
  await new Load(item).save();
};

module.exports.getUserActiveLoad = async (userId) => {
  const res = await Load.where('state')
      .ne(null)
      .ne(undefined)
      .findOne({assigned_to: userId});

  return res;
};

module.exports.dbLoadIterateLoadState = async (userId) => {
  const load = await Load.where('state')
      .ne(null)
      .ne(undefined)
      .findOne({assigned_to: userId});

  if (!load) throw new BadRequestError('No active loads');

  console.log(load, load.state);
  let newState = null;
  const currStatusIndex = LOAD_STATE.findIndex((el) => el === load.state);

  if (currStatusIndex < LOAD_STATE.length - 2) {
    newState = LOAD_STATE[currStatusIndex + 1];
    load.state = newState;
  } else if (currStatusIndex === LOAD_STATE.length - 2) {
    newState = LOAD_STATE[currStatusIndex + 1];
    load.state = newState;
    load.status = 'SHIPPED';
  } else {
    throw new BadRequestError('Load is already SHIPPED');
  }

  await load.save();
  return newState;
};

module.exports.dbLoadGetItemById = async (id, userId) => {
  const item = await Load.findById(id);
  verifyBelong(userId, item.created_by);

  return item;
};

module.exports.dbLoadUpdateItemById = async (id, userId, load) => {
  const item = await Load.findById(id);
  verifyBelong(userId, item.created_by);

  Object.keys(load).forEach((key) => {
    item[key] = load[key];
  });

  await item.save();
};

module.exports.dbLoadDeleteItemById = async (id, userId) => {
  const item = await Load.findById(id);
  verifyBelong(userId, item.created_by);

  await item.remove();
};

module.exports.dbLoadPostItemById = async (id, userId) => {
  const load = await Load.findById(id);
  verifyBelong(userId, load.created_by);

  if (load.status !== 'NEW') {
    throw new BadRequestError('Load is already posted');
  }

  load.status = 'POSTED';
  load.logs.push({message: 'Load posted', time: Date.now()});
  await load.save();

  const fittingTruckTypes = TRUCK_TYPE.filter((el) => {
    if (el.payload < load.payload) return false;
    if (el.dimensions.width < load.dimensions.width) return false;
    if (el.dimensions.length < load.dimensions.length) return false;
    if (el.dimensions.height < load.dimensions.height) return false;
    return true;
  }).map((el) => el.type);

  if (fittingTruckTypes.length === 0) {
    throw new BadRequestError(`Your load is too big for logistic company`);
  }

  let truckCandidate = null;

  for (const type of fittingTruckTypes) {
    truckCandidate = await Truck.findOne({type, status: 'IS'})
        .where('assigned_to')
        .ne(null);

    if (truckCandidate) break;
  }

  if (truckCandidate) {
    load.state = LOAD_STATE[0];
    load.assigned_to = truckCandidate.assigned_to;
    load.status = 'ASSIGNED';

    load.logs.push({
      message: 'Load posted successfully',
      time: Date.now(),
    });
    await load.save();
    await Truck.findByIdAndUpdate(truckCandidate._id, {status: 'OL'});

    return {message: 'Load posted successfully', driver_found: true};
  } else {
    load.status = 'NEW';

    load.logs.push({
      message: `Driver search failed, load returned to NEW status`,
      time: Date.now(),
    });
    await load.save();

    return {message: 'Load posted failed', driver_found: false};
  }
};

module.exports.dbLoadGetAssignedTruck = async (userId) => {
  if (!userId) throw new BadRequestError('No trucks assigned to load');

  return await Truck.findOne({assigned_to: userId});
};
