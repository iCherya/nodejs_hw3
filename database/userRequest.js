/* eslint-disable camelcase */
const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');
const {verifyUser, verifyPassword} = require('../helpers/dataVerification');
const {validateUser} = require('../helpers/dataValidation');

module.exports.dbUserCreate = async ({email, role, password}) => {
  const user = new User({
    email,
    role,
    password: await bcrypt.hash(password, 10),
  });

  await user.save();
};

module.exports.dbUserGetByEmail = async (email) => {
  return verifyUser(await User.findOne({email}));
};

module.exports.dbUserGetById = async (id) => {
  return verifyUser(await User.findById(id));
};

module.exports.dbUserDelete = async (id) => {
  const user = verifyUser(await User.findById(id));
  await user.remove();
};

module.exports.dbUserUpdatePassword = async (id, oldPassword, newPassword) => {
  const user = verifyUser(await User.findById(id));
  await verifyPassword(oldPassword, user.password);
  await validateUser({
    email: user.email,
    role: user.role,
    password: newPassword,
  });
  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();
};
