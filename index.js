const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const config = require('config');

const {logs} = require('./middleware/logsMiddleware');
const authRouter = require('./routes/authRouter');
const userRouter = require('./routes/userRouter');
const truckRouter = require('./routes/truckRouter');
const loadRouter = require('./routes/loadRouter');

const app = express();
const port = process.env.PORT || 8080;

app.use(express.json());
app.use(morgan('combined'));

app.use(logs);

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

app.use((err, req, res, next) => {
  if (err.statusCode) {
    res.status(err.statusCode).json({message: err.message});
  } else if (err.name === 'ValidationError' || err.name === 'CastError') {
    res.status(400).json({message: err.message});
  } else if (err.code === 11000) {
    res.status(400).json({message: `Data already in use`, data: err.keyValue});
  } else {
    console.log('❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌');
    console.log(err);
    console.log('❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌❌');
    res.status(500).json({message: 'Internal server error'});
  }
});

app.use((req, res) => {
  res.status(404).json({message: 'Not found'});
});

const startServer = async () => {
  const {DB_URL, DB_PARAMS} = config.get('DB');
  await mongoose.connect(DB_URL, DB_PARAMS);

  app.listen(port, () => {
    console.log(`🚀🚀🚀🚀🚀 Server started at port ${port} 🚀🚀🚀🚀🚀`);
  });
};

startServer();
