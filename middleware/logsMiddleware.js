const {Logs} = require('../models/logsModel');

module.exports.logs = async (req, res, next) => {
  const {headers, body, params, url, method, query} = req;

  const logs = new Logs({headers, body, params, url, method, query});

  await logs.save();

  next();
};
