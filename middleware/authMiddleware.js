const {verifyToken} = require('../helpers/dataVerification');
const {BadRequestError} = require('../Errors');
const {getUserDataFromToken} = require('../helpers/tokenManipulation');

module.exports.authMiddleware = async (req, res, next) => {
  if (!req.headers.authorization) throw new BadRequestError('Missed token!');

  const [, token] = req.headers.authorization.split(' ');
  req.user = getUserDataFromToken(verifyToken(token));

  next();
};
