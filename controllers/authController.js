const {validateUser} = require('../helpers/dataValidation');
const {dbUserCreate, dbUserGetByEmail} = require('../database/userRequest');
const {verifyPassword} = require('../helpers/dataVerification');
const {getToken} = require('../helpers/tokenManipulation');

module.exports.register = async (req, res) => {
  const user = req.body;

  await validateUser(user);
  await dbUserCreate(user);

  res.json({message: 'Profile created successfully'});
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;

  const user = await dbUserGetByEmail(email);
  await verifyPassword(password, user.password);

  res.json({
    message: 'Success',
    jwt_token: getToken({_id: user._id, email: user.email, role: user.role}),
  });
};

module.exports.forgotPassword = async (req, res) => {
  res.json({
    message: 'If you were registered, new password sent to your email address',
  });
};
