/* eslint-disable camelcase */
const {validateTruck} = require('../helpers/dataValidation');
const {
  dbTruckGetItems,
  dbTruckAddItem,
  dbTruckGetItemById,
  dbTruckUpdateItemById,
  dbTruckDeleteItemById,
  dbTruckAssignItemById,
} = require('../database/truckRequest');

module.exports.getUsersTrucks = async (req, res) => {
  const {offset = 0, limit = 50} = req.query;

  const skipLimit = {
    skip: parseInt(offset),
    limit: parseInt(limit),
  };

  const {_id} = req.user;

  const trucks = await dbTruckGetItems(_id, skipLimit);

  res.json({trucks});
};

module.exports.addTruckForUser = async (req, res) => {
  const {_id: created_by} = req.user;
  let truck = req.body;

  await validateTruck(truck);

  truck = {
    created_by,
    ...truck,
  };

  await dbTruckAddItem(truck);

  res.json({message: 'Truck created successfully'});
};

module.exports.getUserTruckById = async (req, res) => {
  const {id} = req.params;
  const {_id} = req.user;

  res.json({truck: await dbTruckGetItemById(id, _id)});
};

module.exports.updateUserTruckById = async (req, res) => {
  const truck = req.body;
  await validateTruck(truck);

  const {id} = req.params;
  const {_id} = req.user;
  await dbTruckUpdateItemById(id, _id, truck);

  res.json({message: 'Truck details changed successfully'});
};

module.exports.deleteUserTruckById = async (req, res) => {
  const {id} = req.params;
  const {_id} = req.user;

  await dbTruckDeleteItemById(id, _id);

  res.json({message: 'Truck deleted successfully'});
};

module.exports.assignTruckForUserById = async (req, res) => {
  const {id} = req.params;
  const {_id} = req.user;

  await dbTruckAssignItemById(id, _id);

  res.json({message: 'Truck assigned successfully'});
};
