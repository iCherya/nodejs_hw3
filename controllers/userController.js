const {
  dbUserGetById,
  dbUserDelete,
  dbUserUpdatePassword,
} = require('../database/userRequest');

module.exports.getUserInfo = async (req, res) => {
  const {_id} = req.user;

  // eslint-disable-next-line camelcase
  const {email, created_date} = await dbUserGetById(_id);

  res.json({user: {_id, email, created_date}});
};

module.exports.deleteUser = async (req, res) => {
  const {_id} = req.user;

  await dbUserDelete(_id);

  res.json({message: 'Profile deleted successfully'});
};

module.exports.changeUserPassword = async (req, res) => {
  const {_id} = req.user;
  const {oldPassword, newPassword} = req.body;

  await dbUserUpdatePassword(_id, oldPassword, newPassword);

  res.json({message: 'Password changed successfully'});
};
