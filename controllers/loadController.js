/* eslint-disable camelcase */
const {validateLoad} = require('../helpers/dataValidation');
const {
  dbLoadGetItems,
  dbLoadAddItem,
  getUserActiveLoad,
  dbLoadIterateLoadState,
  dbLoadGetItemById,
  dbLoadUpdateItemById,
  dbLoadDeleteItemById,
  dbLoadPostItemById,
  dbLoadGetAssignedTruck,
} = require('../database/loadRequest');

module.exports.getUserLoads = async (req, res) => {
  const {offset = 0, limit = 50, status} = req.query;

  const skipLimit = {
    skip: parseInt(offset),
    limit: parseInt(limit),
  };

  const {_id} = req.user;
  const loads = await dbLoadGetItems(_id, skipLimit, status);

  res.json({loads});
};

module.exports.addLoadForUser = async (req, res) => {
  const {_id} = req.user;
  let load = req.body;

  await validateLoad(load);

  load = {
    created_by: _id,
    ...load,
  };

  await dbLoadAddItem(load);

  res.json({message: 'Load created successfully'});
};

module.exports.getUserActiveLoad = async (req, res) => {
  const {_id} = req.user;

  const load = await getUserActiveLoad(_id);

  res.json({load});
};

module.exports.iterateToNextLoadState = async (req, res) => {
  const {_id} = req.user;

  const newStatus = await dbLoadIterateLoadState(_id);
  res.json({message: `Load state changed to '${newStatus}'`});
};

module.exports.getUserLoadById = async (req, res) => {
  const {id} = req.params;
  const {_id} = req.user;

  res.json({load: await dbLoadGetItemById(id, _id)});
};

module.exports.updateUserLoadById = async (req, res) => {
  const load = req.body;
  await validateLoad(load);

  const {id} = req.params;
  const {_id} = req.user;
  await dbLoadUpdateItemById(id, _id, load);

  res.json({message: 'Load details changed successfully'});
};

module.exports.deleteUserLoadById = async (req, res) => {
  const {id} = req.params;
  const {_id} = req.user;

  await dbLoadDeleteItemById(id, _id);

  res.json({message: 'Load deleted successfully'});
};

module.exports.postUserLoadById = async (req, res) => {
  const {id} = req.params;
  const {_id} = req.user;

  res.json(await dbLoadPostItemById(id, _id));
};

module.exports.getUserLoadShippingInfoById = async (req, res) => {
  const {id} = req.params;
  const {_id} = req.user;

  const load = await dbLoadGetItemById(id, _id);
  const truck = await dbLoadGetAssignedTruck(load.assigned_to);

  res.json({load, truck});
};
