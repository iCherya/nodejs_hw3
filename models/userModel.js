const config = require('config');
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
    enum: config.get('USER_ROLE'),
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.User = mongoose.model('User', userSchema);
