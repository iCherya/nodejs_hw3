const mongoose = require('mongoose');

const logsSchema = new mongoose.Schema({
  headers: {type: Array},
  body: {type: Object},
  params: {type: Object},
  url: {type: String},
  method: {type: String},
  query: {type: Object},
  created_date: {type: Date, default: Date.now()},
});

module.exports.Logs = mongoose.model('Logs', logsSchema);
