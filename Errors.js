/**
 * An Error to be thrown in case of Bad Request Error
 */
class BadRequestError extends Error {
  /**
   * Constructing custom Error
   * @param {String} message Message containing Error specifics
   */
  constructor(message = 'Bad request') {
    super(message);
    this.name = 'BadRequestError';
    this.statusCode = 400;
  }
}

/**
 * An Error to be thrown in case of Server Error
 */
class ServerError extends Error {
  /**
   * Constructing custom Error
   * @param {String} message Message containing Error specifics
   */
  constructor(message = 'Internal server error') {
    super(message);
    this.name = 'ServerError';
    this.statusCode = 500;
  }
}

module.exports = {BadRequestError, ServerError};
