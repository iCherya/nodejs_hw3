# NodeJS_HW3

UBER like service for freight trucks, in REST style, using MongoDB as database. This service should help regular people to deliver their stuff and help drivers to find loads and earn some money. Application contains 2 roles, driver and shipper.

## Available Scripts

In the project directory, you can run:

- `npm i` - to upload all dependencies
- `npm run start` - to start the application
- `npm run dev` - to launch the application in dev mode

## Documentation

API documentation available in [swagger.yaml](./swagger.yaml)
