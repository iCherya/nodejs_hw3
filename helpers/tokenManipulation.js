const jwt = require('jsonwebtoken');
const config = require('config');
const JWT_SECRET = config.get('JWT_SECRET');
const {BadRequestError} = require('../Errors');

module.exports.getToken = (userData) => {
  return jwt.sign(userData, JWT_SECRET);
};

module.exports.getUserDataFromToken = (token) => {
  try {
    return jwt.verify(token, JWT_SECRET);
  } catch (err) {
    throw new BadRequestError('Invalid JWT token!');
  }
};
