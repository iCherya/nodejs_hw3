const {BadRequestError} = require('../Errors');
const bcrypt = require('bcrypt');

module.exports = {
  verifyToken: (token) => {
    if (token) return token;
    throw new BadRequestError('Wrong JWT token!');
  },
  verifyPassword: async (...passwords) => {
    if (await bcrypt.compare(...passwords)) return true;
    throw new BadRequestError('Wrong username or password!');
  },
  verifyUser: (user) => {
    if (user) return user;
    throw new BadRequestError('Wrong username or password!');
  },
  verifyBelong: (userId, belongToUserId) => {
    if (userId === belongToUserId) return true;
    throw new BadRequestError('This is not belong to you');
  },
};
