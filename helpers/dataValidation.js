const Joi = require('joi');

const validateSchemaWrapper = async (objectSchema, objectInstance) =>
  objectSchema.validateAsync(objectInstance);

const schemas = {
  user: Joi.object().keys({
    email: Joi.string().email().required(),
    role: Joi.string().required().valid('SHIPPER', 'DRIVER'),
    password: Joi.string().required().min(3).max(30),
  }),
  truck: Joi.object().keys({
    type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'),
  }),
  load: Joi.object().keys({
    name: Joi.string(),
    payload: Joi.number().min(0),
    pickup_address: Joi.string(),
    delivery_address: Joi.string(),
    dimensions: Joi.object().keys({
      width: Joi.number().min(0),
      length: Joi.number().min(0),
      height: Joi.number().min(0),
    }),
  }),
};

module.exports.validateUser = async (instance) =>
  await validateSchemaWrapper(schemas.user, instance);

module.exports.validateTruck = async (instance) =>
  await validateSchemaWrapper(schemas.truck, instance);

module.exports.validateLoad = async (instance) =>
  await validateSchemaWrapper(schemas.load, instance);
